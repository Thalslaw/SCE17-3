use anyhow::{Context, Result};
use crossterm::execute;
use crossterm::terminal::{Clear, ClearType};
use std::fs::OpenOptions;
use std::io::{self, Write, BufReader, BufRead};
use std::process::{Command, Child};
use std::thread;
use std::time::Duration;
use crossterm::event::{self, Event, KeyCode, KeyEvent};
use crossterm::ExecutableCommand;

fn main() -> Result<()> {
    // Open new terminal windows and store their handles
    let jobs_terminal = open_new_terminal("jobs")?;        // Terminal for Jobs
    let inventory_terminal = open_new_terminal("inventory")?;   // Terminal for Inventory
    let action_queue_terminal = open_new_terminal("action_queue")?; // Terminal for Action Queue
    let agents_terminal = open_new_terminal("agents")?;      // Terminal for Agents

    // Clear the terminal screen
    let mut stdout = io::stdout();
    stdout.execute(Clear(ClearType::All)).context("Failed to clear the terminal")?;

    // Start logging thread
    thread::spawn(move || {
        let log_file = "log.txt"; // This file will be used for communication
        loop {
            let file = OpenOptions::new()
                .read(true)
                .open(log_file)
                .unwrap();

            let reader = BufReader::new(file);
            for line in reader.lines() {
                if let Ok(content) = line {
                    println!("{}", content); // Print log updates to the main terminal
                }
            }
            thread::sleep(Duration::from_secs(1)); // Adjust as necessary
        }
    });

    // Run the main loop to handle events
    run_event_loop(jobs_terminal, inventory_terminal, action_queue_terminal, agents_terminal)?;

    Ok(())
}

fn run_event_loop(jobs_terminal: Child, inventory_terminal: Child, action_queue_terminal: Child, agents_terminal: Child) -> Result<()> {
    // Clear the terminal
    execute!(std::io::stdout(), Clear(ClearType::All)).context("Failed to clear the terminal")?;
    
    // Display the UI. Currently needs rework. <_ RIGHT HERE 
    display_jobs_section()?;
    display_inventory_section()?;
    display_action_queue_section()?;
    display_agents_section()?;

    // Wait for an event
    if event::poll(std::time::Duration::from_millis(500))? {
        if let Event::Key(key_event) = event::read()? {
            handle_key_event(key_event)?; // Call the event handling function

            // Log the key event to the log file
            let mut log_file = OpenOptions::new()
                .write(true)
                .append(true)
                .create(true)
                .open("log.txt")?; // Log file path

            writeln!(log_file, "Key pressed: {:?}", key_event.code)?;

            // Update the respective terminal based on the key event or other logic
            // Example: You could send a command or message to the terminal here

            // Exit on 'Esc' key
            if key_event.code == KeyCode::Esc {
                return Ok(()); // Exit the function
            }
        }
    }

    // Call the function recursively to continue the event loop
    run_event_loop(jobs_terminal, inventory_terminal, action_queue_terminal, agents_terminal) // Recursive call
}

// This function should be modified to return a Child process
fn open_new_terminal(binary_name: &str) -> Result<Child> {
    // Open a new terminal window
    #[cfg(target_os = "linux")]
    {
        Command::new("gnome-terminal")
            .arg("--")
            .arg("sh")
            .arg("-c")
            .arg(format!("cargo run --bin {}", binary_name)) // Replace with your binary name
            .spawn()
            .context("Failed to open new terminal window")
    }

    #[cfg(target_os = "macos")]
    {
        Command::new("open")
            .arg("-a")
            .arg("Terminal")
            .arg(format!("cargo run --bin {}", binary_name)) // Replace with your binary name
            .spawn()
            .context("Failed to open new terminal window")
    }

    #[cfg(target_os = "windows")]
    {
        Command::new("cmd")
            .arg("/C")
            .arg(format!("start cmd /K cargo run --bin {}", binary_name)) // Replace with your binary name
            .spawn()
            .context("Failed to open new terminal window")
    }
}

fn handle_key_event(key_event: KeyEvent) -> Result<()> {
    match key_event {
        KeyEvent {
            code: KeyCode::Esc, // Exit on 'Esc'
            ..
        } => {
            // Exit the recursion
            std::process::exit(0);
        }
        KeyEvent {
            code: KeyCode::Char('p'), // Handle Play action
            ..
        } => {
            println!("Play action clicked!");
            // Add your action handling code here
        }
        KeyEvent {
            code: KeyCode::Char('q'), // Handle Queue action
            ..
        } => {
            println!("Queue action clicked!");
            // Add your action handling code here
        }
        _ => {}
    }

    Ok(())
}

fn display_jobs_section() -> Result<()> {
    let jobs_header = "+-----------------+----------------------+----------------------+";
    let jobs_title = "| Jobs                                                          |";
    let jobs_sub_header = "| Skill           | Job                  | Action               |";

    // Print the headers
    println!("{}", jobs_header);
    println!("{}", jobs_title);
    println!("{}", jobs_header);
    println!("{}", jobs_sub_header);
    println!("{}", jobs_header);

    let jobs = vec![
        ("🛠️", "Discover Information On Trends", "▶️ Play / 📥 Queue", "40%"),
        ("📝", "Finance leverage from Bank", "▶️ Play / 📥 Queue", "100%"),
        ("⚙️", "Initiate training", "▶️ Play / 📥 Queue", "30%"),
        ("🛡️", "Develop Security", "▶️ Play / 📥 Queue", "75%"),
        ("🛡️", "Develop Rights", "▶️ Play / 📥 Queue", "75%"),
        ("🛡️", "Raise Pressure", "▶️ Play / 📥 Queue", "75%"),
    ];

    // Use `map` to transform job tuples into formatted strings
    let job_lines: Vec<String> = jobs.iter().map(|(skill, job, action, status)| {
        // Calculate progress
        let progress = status.trim_end_matches('%').parse::<usize>().unwrap_or(0);
        let progress_bar_length = 30; // Total length of the progress bar
        let filled_length = (progress * progress_bar_length / 100).min(progress_bar_length);

        // Create the progress bar string
        let progress_bar = "=".repeat(filled_length) + &" ".repeat(progress_bar_length - filled_length);

        // Return the formatted job string
        format!(
            "| {:<5} | {:<40} | {:<20} |\n|  [{}]  | {:<10} |",
            skill, job, action, progress_bar, status
        )
    }).collect();

    // Print all job lines
    println!("{}", job_lines.join("\n"));
    
    println!("{}", jobs_header); // Print the footer

    Ok(())
}

fn display_inventory_section() -> Result<()> {
    let inventory_header = "+-----------------+----------------------+----------------------+";
    let inventory_title = "| Inventory                                                     |";
    let inventory_sub_header = "| Item            | Amount               | Notes                |";

    // Print the headers
    println!("{}", inventory_header);
    println!("{}", inventory_title);
    println!("{}", inventory_header);
    println!("{}", inventory_sub_header);
    println!("{}", inventory_header);

    let inventory = vec![
        ("Unsorted Files", "1", "Admin/1"),
        ("Admin Files", "1", "Admin/0"),
        ("Workforce Files", "1", "Broker/0"),
        ("QualityControl Files", "1", "Admin/1(QualityControl)"),
        ("Brain Trust Files", "1", "Science/0"),
        ("Banking Files", "1", "Admin/1(Banking)"),
        ("Banked Leverage", "4", "USER ONLY"),
    ];

    // Use `map` to transform inventory tuples into formatted strings
    let inventory_lines: Vec<String> = inventory.iter().map(|(item, amount, notes)| {
        format!(
            "| {:<17} | {:<10} | {:<30} |",
            item, amount, notes
        )
    }).collect();

    // Print all inventory lines
    println!("{}", inventory_lines.join("\n"));
    
    // Print the footer
    println!("{}", inventory_header);

    Ok(())
}


fn display_action_queue_section() -> Result<()> {
    let action_queue_header = "+-----------------+----------------------+----------------------+";
    let action_queue_title = "| Action Queue                                                  |";
    
    println!("{}", action_queue_header);
    println!("{}", action_queue_title);
    println!("{}", action_queue_header);
    println!("| None                                                          |");
    println!("{}", action_queue_header);

    Ok(())
}

fn display_agents_section() -> Result<()> {
    let agents_header = "+-----------------+----------------------+----------------------+";
    let agents_title = "| Agents                                                       |";
    let agents_sub_header = "| Agent Name      | Current Job         | Status               |";

    println!("{}", agents_header);
    println!("{}", agents_title);
    println!("{}", agents_header);
    println!("{}", agents_sub_header);
    println!("{}", agents_header);

    let agents = vec![
        ("🧑‍💼 John Doe", "Discover Info", "Working"),
        ("🧑‍💼 Jane Smith", "Develop Security", "Idle"),
        ("🧑‍💼 Alex Brown", "Initiate Training", "Working"),
    ];

    for (name, job, status) in agents {
        println!("| {:<17} | {:<20} | {:<20} |", name, job, status);
        println!("{}", agents_header);
    }

    Ok(())
}
